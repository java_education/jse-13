package ru.t1.oskinea.tm.controller;

import ru.t1.oskinea.tm.api.controller.IProjectTaskController;
import ru.t1.oskinea.tm.api.service.IProjectTaskService;
import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.model.Task;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(projectId, taskId);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
