package ru.t1.oskinea.tm.api.repository;

import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    boolean existsById(String id);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task create(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}
